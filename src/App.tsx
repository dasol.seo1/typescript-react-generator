import React from 'react';
import './App.scss';

const App = (): JSX.Element => {
    return <h1 className="App">Hello World</h1>;
};

export default App;
